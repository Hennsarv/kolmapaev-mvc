﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnDynamikud
{
    class Inimene
    {
        public string Nimi;
        public int Vanus;
    }


    class Program
    {
        static void Main(string[] args)
        {
            int arv = 7;
            // nimi, andmetüüp, väärtus
            string nimi = "Henn";

            foreach(var m in nimi.GetType().GetProperties())
                Console.WriteLine(m.Name);
            

            dynamic d;
            d = 7;
            Console.WriteLine(d.GetType().Name);
            d = "Sarvik";
            Console.WriteLine(d.GetType().Name);
            d = new Inimene { Nimi = "Henn", Vanus = 63 };
            d = 63;
            

            dynamic MinuTasku = new PropBag();
            MinuTasku.Nimi = "Henn";
            MinuTasku.Vanus = 63;
            MinuTasku.Sisu = "puru";

            Console.WriteLine(MinuTasku.NImi);

            


        }

    }

    class PropBag : DynamicObject
    {
        private Dictionary<string, dynamic> prop = new Dictionary<string, dynamic>();
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            prop[binder.Name] = value;
            return true;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {

            result = prop.ContainsKey(binder.Name) ? prop[binder.Name] : "";
            return true;
        }

    }
}
