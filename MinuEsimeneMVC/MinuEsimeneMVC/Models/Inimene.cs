﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinuEsimeneMVC.Models
{
    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}