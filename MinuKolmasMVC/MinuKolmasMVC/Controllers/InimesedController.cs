﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MinuKolmasMVC.Models;

namespace MinuKolmasMVC.Controllers
{
    public class InimesedController : Controller
    {
        static Dictionary<int,Inimene> Inimesed = new Dictionary<int,Inimene>();


        // GET: Inimesed
        public ActionResult Index()
        {
            // see siin on et esimesel korral oleks mõni
            // igapäevases rakenduses sellist ei tehta
            // ma tegin selle ainult demo mõttes
            //int nr = 1;
            string[] nimed = { "Henn", "Ants", "Peeter" };
            int[] vanused = { 63, 40, 28 };
            if (Inimesed.Count == 0)
                //Inimesed = nimed
                //    .Select(x => new Inimene { Number = nr, Nimi = x, Vanus = vanused[nr++ - 1] })
                //    .ToDictionary(x => x.Number, x => x);
                // );

                for (int nr = 0; nr < nimed.Length; nr++)
                    Inimesed.Add(nr+1, 
                        new Inimene 
                        {
                            Number = nr + 1,
                            Nimi = nimed[nr],
                            Vanus = vanused[nr]
                        }
                        );
        


            // see on standardne lause
            return View(Inimesed.Values);
        }

        // GET: Inimesed/Details/5
        public ActionResult Details(int id)
        {
            if (Inimesed.ContainsKey(id))

                return View(Inimesed[id]);


            else return RedirectToAction("Index");
        }

        // GET: Inimesed/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimesed/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimesed/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Inimesed/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimesed/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimesed/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
