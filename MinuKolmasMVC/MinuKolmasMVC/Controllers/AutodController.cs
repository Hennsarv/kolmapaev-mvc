﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MinuKolmasMVC.Controllers
{
    public class AutodController : Controller
    {
        // GET: Autod
        public ActionResult Index()
        {
            return View();
        }

        // GET: Autod/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Autod/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Autod/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Autod/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Autod/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Autod/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Autod/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
