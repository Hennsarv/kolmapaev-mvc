﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MinuKolmasMVC.Models;

namespace MinuKolmasMVC
{
    namespace Models
    {
        partial class Employee
        {
            public string FullName => $"{FirstName} {LastName}";
        }
    }

    namespace Controllers
    {
        public class EmployeesController : Controller
        {
            private NorthwindEntities db = new NorthwindEntities();

            // GET: Employees
            public ActionResult Index()
            {
                var employees = db.Employees.Include(e => e.Manager);
                return View(employees.ToList());
            }

            public string FullName(int? id)
            {
                return db.Employees.Find(id ?? 0)?.FullName ?? "puudub";
            }

            public ActionResult Foto(int? id)
            {
                if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                Employee e = db.Employees.Find(id.Value);
                if (e == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                var picture = e.Photo;
                if (picture == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                // see rida on vajalik, kui on tegu vanade mustvalgete filmidega :)
                // vanad BMP stiilis baidijadad 
                // tunneb ära, et esimene bait on 21 siis jätab 78 baiti vahele
                if (picture[0] == 21) picture = picture.Skip(78).ToArray();
                return File(picture, "image/jpg");



            }


            // GET: Employees/Details/5
            public ActionResult Details(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee = db.Employees.Find(id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                return View(employee);
            }

            // GET: Employees/Create
            public ActionResult Create()
            {
                ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName");
                return View();
            }

            // POST: Employees/Create
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Create([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
            {
                if (ModelState.IsValid)
                {
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
                return View(employee);
            }

            // GET: Employees/Edit/5
            public ActionResult Edit(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee = db.Employees.Find(id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
                return View(employee);
            }

            // POST: Employees/Edit/5
            // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
            // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Edit([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(employee).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "LastName", employee.ReportsTo);
                return View(employee);
            }

            // GET: Employees/Delete/5
            public ActionResult Delete(int? id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Employee employee = db.Employees.Find(id);
                if (employee == null)
                {
                    return HttpNotFound();
                }
                return View(employee);
            }

            // POST: Employees/Delete/5
            [HttpPost, ActionName("Delete")]
            [ValidateAntiForgeryToken]
            public ActionResult DeleteConfirmed(int id)
            {
                Employee employee = db.Employees.Find(id);
                db.Employees.Remove(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                base.Dispose(disposing);
            }
        }
    }
}