﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using MinuKolmasMVC.Models;
//using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MinuKolmasMVC.Models;

namespace MinuKolmasMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Mina olin enne index";
            ViewBag.Brauser = Request.Browser.Type;

            Models.NorthwindEntities db = new Models.NorthwindEntities();
            ViewBag.Kliente = db.Customers.Count();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            NorthwindEntities db = new NorthwindEntities();

            // ei olnudki midagi puudu, lihtsalt tuli jõuga kirjutada lambda avaldis
            // nimelt on kaks erinevat Include'i - üks üldine ja teine generic

            // kui kirjutada Include( - siis ta arvab, et tegu on esimesega
            // kui kirjutada Include<Employee,Employee>( - siis et teisega
            // kui kirjutada Include(x => x.Manager) siis ta ei arva vaid saab aru

            var employees = db.Employees.Include(x => x.Manager);
            var employees2 = db.Employees.Include<Employee, Employee>(x => x.Manager);
            ViewBag.Töötajad = employees.ToList();

            // ehh kurat - ta töötab ka ilma selle includita - viimast on vaja, kui ma seal
            // listis selle Manageriga midagi ette võtta tahaks

            return View();
        }
    }
}