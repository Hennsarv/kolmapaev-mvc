﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinuKolmasMVC.Models
{
    public class Auto
    {
        public string Mudel { get; set; }
        public int VäljalaskeAasta { get; set; }
        public string Värv { get; set; }

    }
}