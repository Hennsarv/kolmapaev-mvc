﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinuKolmasMVC.Models
{
    public class Inimene
    {
        public int Number { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}