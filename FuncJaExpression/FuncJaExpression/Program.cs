﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FuncJaExpression
{
    class Program
    {
        public static int Kuup(int millest)
        {
            return millest * millest * millest;
        }

        static void Main(string[] args)
        {
            Func<int, int> ruut = (x) => x* x; 

            Console.WriteLine(ruut);

            ruut = Kuup;
            Console.WriteLine(ruut(5));


            Console.WriteLine(ruut(4));

            Expression<Func<int, int>> exp = x => x * x + 7;

            Console.WriteLine(exp.ToString());

            var ef = exp.Compile();

            Console.WriteLine(ef(7));
            Console.WriteLine(exp.Compile()(9));

            northwind22Entities db = new northwind22Entities();
            db.Database.Log = Console.WriteLine;

            foreach(var p in db.Products
                //.ToList()
                .Where(x => x.UnitPrice < 10)
                .Include(x => x.Category)
                .OrderBy(x => x.UnitPrice)
                .Select(x => new {x.Category.CategoryName, x.ProductName})
                )
                Console.WriteLine(p.ProductName + " " + p.CategoryName);

            var q = from x in db.Products
                    where x.UnitPrice < 10
                    orderby x.UnitPrice
                    select new { x.ProductName, x.Category.CategoryName };
            foreach (var p in q) Console.WriteLine(p);

            var q1 = from c in db.Categories
                     select new
                     {
                         c.CategoryName,
                         keskmine = (
                            from p in c.Products
                            where p.Discontinued == false
                            select p.UnitPrice).Average()
                     };
            foreach (var x in q1) Console.WriteLine(x);

            var cat = db.Categories.Find(8);
            System.IO.File.WriteAllBytes($"..\\..\\{cat.CategoryName}.jpg", cat.Picture);

             // näitamiseks teen muudatuse               
                     
                     

        }
    }
}
