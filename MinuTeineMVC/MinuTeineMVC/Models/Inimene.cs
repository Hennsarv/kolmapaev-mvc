﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinuTeineMVC.Models
{
    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }
    }
}